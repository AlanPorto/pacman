﻿using UnityEngine;
using System.Collections;

public class Pill : MonoBehaviour
{
	private GameManager gameRef;

	public void Init(GameManager game)
	{
		gameRef = game;
	}

	void OnTriggerEnter(Collider obj)
	{
		if (obj.gameObject.GetComponent<Pacman>() != null)
			gameRef.PacmanGetsPill (this);
	}

}
