﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{
	// --- Public
	public GameObject wallPrefab;
	public Pill pillPrefab;
	public Pacman pacmanPrefab;
	public Ghost ghostPrefab;
	public UILabel txt_score;

	// --- Private
	private Pacman pacman;
	private int score;
	private List<Pill> pillList;

	void Start()
	{
		score = 0;
		pillList = new List<Pill> ();
		Level.ResetMap ();
		CreateMap ();

		ShowScore ();
	}

	public void PacmanGetsPill(Pill pill)
	{
		pillList.Remove (pill);
		DestroyObject (pill.gameObject);

		score += 10;

		ShowScore ();

		if (pillList.Count <= 0)
			WinGame ();
	}

	private void ShowScore()
	{
		txt_score.text = "Score: " + score.ToString ();
	}

	public void WinGame()
	{
		Application.LoadLevel("WinScene");
	}

	public void GameOver()
	{
		Application.LoadLevel("GameOverScene");
	}

	public Pacman GetPacman()
	{
		return pacman;
	}

	private void CreateMap()
	{
		int gridSizeX = Level.GetMapRowSize ();
		int gridSizeY = Level.GetMapColSize ();

		for (int i = 0; i < gridSizeX; i++)
		{
			for (int j = 0; j < gridSizeY; j++)
			{
				Vector3 gridPosition = Level.GetSnapPositionALL(i, j);
				
				if (Level.GetMapValue(i, j) == Level.WALL)
				{
					GameObject wall = Instantiate (wallPrefab) as GameObject;
					
					wall.transform.parent = this.transform;
					wall.transform.localPosition = gridPosition;
				}
				else if (Level.GetMapValue(i, j) == Level.EMPTY)
				{
					Pill tempPill = Instantiate (pillPrefab) as Pill;
					tempPill.transform.parent = this.transform;
					tempPill.transform.localPosition = gridPosition;
					tempPill.Init(this);
					pillList.Add(tempPill);
				}
				else if (Level.GetMapValue(i, j) == Level.PACMAN)
				{
					pacman = Instantiate (pacmanPrefab) as Pacman;
					pacman.Init(this, i, j);
					
					pacman.transform.parent = this.transform;
					pacman.transform.localPosition = gridPosition;
				}
				else if (Level.GetMapValue(i, j) == Level.GHOST)
				{
					Ghost ghost = Instantiate (ghostPrefab) as Ghost;
					ghost.Init(this, i, j);
					
					ghost.transform.parent = this.transform;
					ghost.transform.localPosition = gridPosition;
				}
			}
		}
	}

}
