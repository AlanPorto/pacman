﻿using UnityEngine;
using System.Collections;

public class Ghost : Character
{
	public Material chaseMaterial;
	public Material neutralMaterial;

	private bool isChasingPlayer;

	GridPos[] directions = new GridPos[]
	{
		new GridPos(0, 1),
		new GridPos(0, -1),
		new GridPos(1, 0),
		new GridPos(-1, 0)
	};

	private void Start()
	{
		isChasingPlayer = false;
	}
	
	private void Update()
	{
		if (direction.x == 0 && direction.y == 0)
		{
			if (Random.Range (0, 10) <= 6)
				isChasingPlayer = true;
			else
				isChasingPlayer = false;

			if (!isChasingPlayer)
				direction = directions [Random.Range(0, 4)];
			else
			{
				GridPos pacmancPos = gameRef.GetPacman().GetPosInGrid();

				if (pacmancPos.x < this.posInGrid.x)
					direction = new GridPos(-1, 0);
				else if (pacmancPos.x > this.posInGrid.x)
					direction = new GridPos(1, 0);
				else if (pacmancPos.y < this.posInGrid.y)
					direction = new GridPos(0, -1);
				else if (pacmancPos.y > this.posInGrid.y)
					direction = new GridPos(0, 1);
				else
					direction = directions [Random.Range(0, 4)];
			}

			ChangeMaterial();
		}

		Move ();
	}


	void ChangeMaterial()
	{
		if (isChasingPlayer)
			this.renderer.material = chaseMaterial;
		else
			this.renderer.material = neutralMaterial;
	}

	void OnTriggerEnter(Collider obj)
	{
		if (obj.gameObject.GetComponent<Pacman> () != null)
			gameRef.GameOver ();
	}
}
