﻿using UnityEngine;
using System.Collections;

public class UIManager : MonoBehaviour
{
	public void OnClickStart()
	{
		Application.LoadLevel("GameScene");
	}

	public void OnClickBackToMenu()
	{
		Application.LoadLevel("MainScene");
	}
}
