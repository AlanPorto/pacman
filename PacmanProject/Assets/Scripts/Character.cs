﻿using UnityEngine;
using System.Collections;

public class Character : MonoBehaviour
{
	// --- Public
	public float speed;

	// --- Private
	protected GridPos posInGrid;
	protected GridPos targetPosInGrid;
	protected GridPos direction = new GridPos(0, 0);
	
	protected GridPos lastDirection = new GridPos(0,0);
	
	private Vector3 position;
	private Vector3 targetPosition;
	
	private float moveTimer = Mathf.Infinity;

	protected GameManager gameRef;
	
	public virtual void Init(GameManager game, int x, int y)
	{
		gameRef = game;

		posInGrid = new GridPos(x, y);
		targetPosInGrid = posInGrid;
	}

	public GridPos GetPosInGrid()
	{
		return posInGrid;
	}

	protected void Move()
	{
		CheckDirection ();

		if (direction.x == 0 && direction.y == 0)
			return;
		
		if (moveTimer >= 1.0f)
		{
			posInGrid = new GridPos(targetPosInGrid.x, targetPosInGrid.y);	

			position = Level.GetSnapPositionALL(posInGrid.x, posInGrid.y);

			targetPosInGrid = new GridPos(posInGrid.x + direction.x, posInGrid.y + direction.y);

			if (Level.GetMapValue(targetPosInGrid.x, targetPosInGrid.y) == 1)
			{
				direction = new GridPos(0, 0);
				targetPosInGrid = new GridPos(posInGrid.x, posInGrid.y);
				return;
			}

			targetPosition = Level.GetSnapPositionALL(targetPosInGrid.x, targetPosInGrid.y);
			
			moveTimer = 0f;
		} 
		
		moveTimer += Time.deltaTime * speed;
		lastDirection = direction;
		this.transform.position = Vector3.Lerp(position, targetPosition, moveTimer);

	}

	private void CheckDirection()
	{
		if (Level.GetMapValue(direction.x + targetPosInGrid.x, direction.y + targetPosInGrid.y) == 1)
			direction = lastDirection;
	}
}










