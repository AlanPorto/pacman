﻿using UnityEngine;
using System.Collections;

public class Pacman : Character
{
	private int score;
	
	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.UpArrow))
		{
			direction = new GridPos(-1, 0);
		}
		else if (Input.GetKeyDown(KeyCode.DownArrow))
		{
			direction = new GridPos(1, 0);
		}
		else if (Input.GetKeyDown(KeyCode.LeftArrow))
		{
			direction = new GridPos(0, -1);
		}
		else if (Input.GetKeyDown(KeyCode.RightArrow))
		{
			direction = new GridPos(0, 1);
		}


		FixRotation ();
		Move ();
	}

	private void FixRotation()
	{
		Quaternion rotation = this.transform.rotation;

		if (direction.x == 1 && direction.y == 0)
		{
			rotation.eulerAngles = new Vector3 (0, 180, 0);
		}
		else if (direction.x == -1 && direction.y == 0)
		{
			rotation.eulerAngles = new Vector3 (0, 0, 0);
		}
		else if (direction.x == 0 && direction.y == 1)
		{
			rotation.eulerAngles = new Vector3 (0, 90, 0);
		}
		else if (direction.x == 0 && direction.y == -1)
		{
			rotation.eulerAngles = new Vector3 (0, -90, 0);
		}

		this.transform.localRotation = rotation;
		
	}

}
